package com.challenge.data;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DataClassTest {

    private DataClass dataClass;

    @BeforeEach
    void setUp() {
        dataClass = new DataClass();
    }

    @Test
    void testSetGetClassName_shouldReturnCorrectValue() {
        String expected = "Kelas BBB";
        dataClass.setClassName(expected);
        Assertions.assertEquals(expected, dataClass.getClassName());
    }

    @Test
    void testSetGetGrades_shouldReturnCorrectValue() {
        List<Integer> expected = Arrays.asList(8, 8, 9, 10, 9, 7);
        dataClass.setGrades(expected);
        Assertions.assertEquals(expected, dataClass.getGrades());
    }
}