package com.challenge.generator;

import com.challenge.data.DataClass;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataClassFeeder {
    public static List<DataClass> createDataClassList() {
        List<DataClass> dataClassList = new ArrayList<>();
        DataClass dataClassA = new DataClass();
        dataClassA.setClassName("Kelas A");
        // mean is 8.9
        dataClassA.setGrades(Arrays.asList(8, 8, 8, 8, 9, 9, 9, 10, 10, 10));
        dataClassList.add(dataClassA);

        DataClass dataClassB = new DataClass();
        dataClassB.setClassName("Kelas B");
        // mean is 8.3
        dataClassB.setGrades(Arrays.asList(6, 7, 7, 8, 8, 9, 9, 9, 10, 10));
        dataClassList.add(dataClassB);

        DataClass dataClassC = new DataClass();
        dataClassC.setClassName("Kelas C");
        // mean is 7.7
        dataClassC.setGrades(Arrays.asList(6, 6, 7, 7, 8, 8, 8, 9, 9, 9));
        dataClassList.add(dataClassC);
        return dataClassList;
    }
}
