package com.challenge.generator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

class GradesFrequenciesGeneratorTest {

    @Mock
    private Calculator calculator;

    private GradesFrequenciesGenerator generator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        generator = new GradesFrequenciesGenerator(calculator);
    }

    @AfterEach
    void tearDown() {
        Mockito.verifyNoMoreInteractions(calculator);
    }

    @Test
    void testCreateTable_givenListFromFeeder_shouldCalculateFrequencyFromCalculator_andGenerateColumn() {
        Mockito.when(calculator.getFrequency(any(), anyInt())).thenReturn(1);
        generator.createTable(DataClassFeeder.createDataClassList());
        Mockito.verify(calculator, Mockito.times(12)).getFrequency(any(), anyInt());
    }
}