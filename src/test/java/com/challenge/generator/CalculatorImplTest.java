package com.challenge.generator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class CalculatorImplTest {

    private static CalculatorImpl calculator;

    @BeforeAll
    static void setUp() {
        calculator = new CalculatorImpl();
    }

    @Test
    void testGetFrequency_shouldReturnCorrectFrequency() {
        Assertions.assertEquals(3, calculator.getFrequency(createRawGrades(), 1));
        Assertions.assertEquals(3, calculator.getFrequency(createRawGrades(), 2));
        Assertions.assertEquals(4, calculator.getFrequency(createRawGrades(), 3));
        Assertions.assertEquals(5, calculator.getFrequency(createRawGrades(), 4));
    }

    @Test
    void testGetMeanPerClass_shouldReturnCorrectValue() {
        HashMap<String, Double> meanPerClass = calculator.getMeanPerClass(DataClassFeeder.createDataClassList());
        String classA = "Kelas A";
        double classAMean = 8.9;
        Assertions.assertEquals(classAMean, meanPerClass.get(classA));

        String classB = "Kelas B";
        double classBMean = 8.3;
        Assertions.assertEquals(classBMean, meanPerClass.get(classB));

        String classC = "Kelas C";
        double classCMean = 7.7;
        Assertions.assertEquals(classCMean, meanPerClass.get(classC));
    }

    @Test
    void testGetMedian_shouldReturnCorrectValue() {
        List<Double> values = Arrays.asList(5.0, 8.0, 9.0, 9.0, 7.0, 7.0, 5.0, 10.0);
        double expected = 7.5;
        Assertions.assertEquals(expected, calculator.getMedian(values));
    }

    @Test
    void testGetMedian_whenValuesSizeIsFive_shouldReturnCorrectValue() {
        List<Double> values = Arrays.asList(5.0, 8.0, 9.0, 9.0, 7.0);
        double expected = 8.0;
        Assertions.assertEquals(expected, calculator.getMedian(values));
    }

    @Test
    void testGetModus_shouldReturnCorrectValue() {
        List<Double> listAllMean = Arrays.asList(5.0, 8.0, 9.0, 9.0, 7.0, 7.0, 7.0, 5.0, 10.0);
        double expected = 3.0;
        Assertions.assertEquals(expected, calculator.getModus(listAllMean));
    }

    private List<Integer> createRawGrades() {
        return Arrays.asList(1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4);
    }
}