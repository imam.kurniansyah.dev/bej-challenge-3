package com.challenge.generator;

import java.io.File;
import java.util.HashMap;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;

class ModusMedianMeanGeneratorTest {

    @Mock
    private Calculator calculator;

    private ModusMedianMeanGenerator generator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        generator = new ModusMedianMeanGenerator(calculator);
    }

    @AfterEach
    void tearDown() {
        Mockito.verifyNoMoreInteractions(calculator);
    }

    @Test
    void testCreateModusMedianMeanFile_shouldCreateTheFile() {
        HashMap<String, Double> meanPerClass = new HashMap<>();
        meanPerClass.put("Kelas A", 1.0);
        meanPerClass.put("Kelas B", 2.0);
        Mockito.when(calculator.getMedian(any())).thenReturn(1.0);
        Mockito.when(calculator.getModus(any())).thenReturn(1L);
        Mockito.when(calculator.getMeanPerClass(DataClassFeeder.createDataClassList())).thenReturn(meanPerClass);
        generator.createModusMedianMeanFile(DataClassFeeder.createDataClassList());
        File file = new File(Constant.FILE_LOCATION_GRADES_FREQUENCY);
        Assertions.assertTrue(file.exists());
        Mockito.verify(calculator).getMedian(any());
        Mockito.verify(calculator).getModus(any());
        Mockito.verify(calculator).getMeanPerClass(DataClassFeeder.createDataClassList());
    }
}