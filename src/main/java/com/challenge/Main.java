package com.challenge;

import com.challenge.generator.Calculator;
import com.challenge.generator.CalculatorImpl;
import com.challenge.generator.Constant;
import com.challenge.generator.GradesFrequenciesGenerator;
import com.challenge.generator.ModusMedianMeanGenerator;
import java.util.Scanner;

public class Main {

    private static final Calculator calculator = new CalculatorImpl();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        generateMainMenu(scanner);
    }

    private static void generateMainMenu(Scanner scanner) {
        System.out.println(createSeparator());
        System.out.println("Aplikasi Pengolahan Nilai Siswa");
        System.out.println(createSeparator());
        System.out.println("File akan dibaca di " + Constant.FILE_LOCATION);
        System.out.println("File akan digenerate di " + Constant.FILE_LOCATION_GRADES_FREQUENCY);
        System.out.println();
        System.out.println("Silahkan pilih action :");
        System.out.println("1. Kelompokkan data");
        System.out.println("2. Menyajikan mean, median, dan modus");
        System.out.println("0. Keluar");
        System.out.print("Pilih menu : ");
        int selectedMenu = scanner.nextInt();
        selectMenu(scanner, selectedMenu);
    }

    private static void selectMenu(Scanner scanner, int menu) {
        int selectedMenu;
        if (menu == 1) {
            GradesFrequenciesGenerator.getInstance(calculator).generate();
        } else {
            ModusMedianMeanGenerator.getInstance(calculator).generate();
        }
        System.out.println(createSeparator());
        System.out.println("Menu kelompokkan data");
        System.out.println(createSeparator());
        System.out.println("File akan digenerate di " + Constant.FILE_LOCATION_GRADES_FREQUENCY);
        System.out.println();
        System.out.println("1. Kembali ke menu utama");
        System.out.println("0. Keluar");
        System.out.println();
        System.out.print("Pilih menu : ");
        selectedMenu = scanner.nextInt();
        if (selectedMenu == 1) {
            generateMainMenu(scanner);
        } else {
            scanner.close();
        }
    }

    private static String createSeparator() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 40; i++) {
            stringBuilder.append("-");
        }
        return stringBuilder.toString();
    }
}
