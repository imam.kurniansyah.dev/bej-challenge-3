package com.challenge.data;

import java.util.List;
import lombok.Data;

@Data
public class DataClass {
    private String className;
    private List<Integer> grades;
}
