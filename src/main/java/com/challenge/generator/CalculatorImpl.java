package com.challenge.generator;

import com.challenge.data.DataClass;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CalculatorImpl implements Calculator {
    /**
     * @param rawGrades is class grades
     * @param grades is grade to be compare to get frequency
     * @return frequency for the grade
     */
    @Override public int getFrequency(List<Integer> rawGrades, int grades) {
        int frequency = 0;
        for (Integer grade : rawGrades) {
            if (Objects.equals(grade, grades)) {
                frequency++;
            }
        }
        return frequency;
    }

    /**
     * @param dataClassList is Data Class extracted from the File
     * @return Mean for all class
     */
    @Override public HashMap<String, Double> getMeanPerClass(List<DataClass> dataClassList) {
        // store class name as Key, and Mean for that class as Value
        HashMap<String, Double> meanPerClass = new HashMap<>();
        for (DataClass dataClass : dataClassList) {
            List<Integer> grades = dataClass.getGrades();
            float sumGrades = 0;
            for (Integer grade : grades) {
                sumGrades += grade;
            }

            double mean = sumGrades / grades.size();
            // store each Class Name and Mean into map
            meanPerClass.put(dataClass.getClassName(), roundDouble(mean));
        }

        // convert Mean from each class to a List
        return meanPerClass;
    }

    /**
     * @param listAllMean is Mean from all class
     * @return middle value of all Mean
     */
    @Override public double getMedian(List<Double> listAllMean) {
        // to calculate the Median, values should sort first
        Collections.sort(listAllMean);

        if (listAllMean.size() % 2 == 1) {
            return listAllMean.get((listAllMean.size() + 1) / 2 - 1);
        } else {
            double lower = listAllMean.get(listAllMean.size() / 2 - 1);
            double upper = listAllMean.get(listAllMean.size() / 2);

            return (lower + upper) / 2.0;
        }
    }

    /**
     * @param listAllMean is Mean from all class
     * @return max frequency for value
     */
    @Override public long getModus(List<Double> listAllMean) {
        HashMap<Long, Long> frequencyPerValue = new HashMap<>();
        for (int i = 0; i < listAllMean.size(); i++) {
            long key = Math.round(listAllMean.get(i));
            if (i == 0) {
                frequencyPerValue.put(key, 1L);
            } else {
                if (frequencyPerValue.containsKey(key)) {
                    long frequency = frequencyPerValue.get(key) + 1;
                    frequencyPerValue.put(key, frequency);
                } else {
                    frequencyPerValue.put(key, 1L);
                }
            }
        }

        return Collections.max(frequencyPerValue.values());
    }

    /**
     * @param value not rounded value
     * @return rounded value
     */
    private double roundDouble(double value) {
        BigDecimal bigDecimal = BigDecimal.valueOf(value);
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        return bigDecimal.doubleValue();
    }
}
