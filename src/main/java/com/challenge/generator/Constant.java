package com.challenge.generator;

public class Constant {
    private Constant() {

    }

    public static final String FILE_LOCATION = "src/main/resources/data_sekolah.csv";
    public static final String FILE_LOCATION_GRADES_FREQUENCY = "src/main/resources/data_frekuensi_nilai.txt";
    public static final String FILE_LOCATION_MODUS_MEDIAN_MEAN = "src/main/resources/data_modus_median_mean.txt";
}
