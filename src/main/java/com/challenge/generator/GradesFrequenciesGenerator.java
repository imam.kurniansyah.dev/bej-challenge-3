package com.challenge.generator;

import com.challenge.data.DataClass;
import com.google.common.annotations.VisibleForTesting;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class GradesFrequenciesGenerator extends BaseGenerator {

    private static final int MAX_CHAR_CLASS = 7;
    private static final int MAX_CHAR_GRADES = 17;
    private static final int MAX_CHAR_FREQUENCIES = 9;

    private final Calculator calculator;

    private static GradesFrequenciesGenerator instance;

    public GradesFrequenciesGenerator(Calculator calculator) {
        this.calculator = calculator;
    }

    // make Singleton instance
    public static GradesFrequenciesGenerator getInstance(Calculator calculator) {
        if (instance == null) {
            instance = new GradesFrequenciesGenerator(calculator);
        }
        return instance;
    }

    @Override public void generate() {
        generateGrades();
    }

    private void generateGrades() {
        if (dataClasses.size() > 1) {
            createTable(dataClasses);
        } else {
            System.out.println("File yang dibuka kosong");
        }
    }

    @VisibleForTesting
    void createTable(List<DataClass> dataClassList) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter(Constant.FILE_LOCATION_GRADES_FREQUENCY))) {
            generateColumn(bufferedWriter, "KELAS", MAX_CHAR_CLASS);
            generateColumn(bufferedWriter, "NILAI", MAX_CHAR_GRADES);
            generateColumn(bufferedWriter, "FREKUENSI", MAX_CHAR_FREQUENCIES);

            bufferedWriter.newLine();

            dataClassList.forEach(dataClass -> {
                List<Integer> grades = new ArrayList<>(new LinkedHashSet<>(dataClass.getGrades()));
                generateColumn(bufferedWriter, dataClass.getClassName(), MAX_CHAR_CLASS);

                for (int i = 0; i < grades.size(); i++) {
                    try {
                        generateColumn(bufferedWriter, String.valueOf(grades.get(i)), MAX_CHAR_GRADES);
                        // to count frequencies, comparing the value from groupedGrades and value from dataClass
                        int frequency = calculator.getFrequency(dataClass.getGrades(), grades.get(i));
                        generateColumn(bufferedWriter, String.valueOf(frequency), MAX_CHAR_FREQUENCIES);
                        bufferedWriter.newLine();

                        // prevent print class name on the end of the dataClass
                        if (i < grades.size() - 1) {
                            generateColumn(bufferedWriter, dataClass.getClassName(), MAX_CHAR_CLASS);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            bufferedWriter.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void generateColumn(BufferedWriter bufferedWriter, String name, int maxChar) {
        try {
            bufferedWriter.write(name);
            for (int i = 0; i < maxChar - name.length(); i++) {
                bufferedWriter.write(" ");
            }
            bufferedWriter.write(" | ");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
