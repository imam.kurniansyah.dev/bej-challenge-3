package com.challenge.generator;

import com.challenge.data.DataClass;
import com.google.common.annotations.VisibleForTesting;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModusMedianMeanGenerator extends BaseGenerator {

    private final Calculator calculator;

    private static ModusMedianMeanGenerator instance;

    public ModusMedianMeanGenerator(Calculator calculator) {
        this.calculator = calculator;
    }

    // make Singleton instance
    public static ModusMedianMeanGenerator getInstance(Calculator calculator) {
        if (instance == null) {
            instance = new ModusMedianMeanGenerator(calculator);
        }
        return instance;
    }

    @Override public void generate() {
        generateGrades();
    }

    private void generateGrades() {
        if (dataClasses != null) {
            createModusMedianMeanFile(dataClasses);
        } else {
            System.err.println("File yang dibuka kosong");
        }
    }

    @VisibleForTesting
    void createModusMedianMeanFile(List<DataClass> dataClasses) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter(Constant.FILE_LOCATION_MODUS_MEDIAN_MEAN))) {
            // get Mean for each Class. ex: KelasA to 8 <-- mean
            HashMap<String, Double> meanPerClass = calculator.getMeanPerClass(dataClasses);
            // convert Mean from each class to a List
            ArrayList<Double> listAllMean = new ArrayList<>(meanPerClass.values());
            // sum Mean from all class Mean
            double totalMean = 0.0;
            for (double mean : listAllMean) {
                totalMean += mean;
            }

            // get Mean from all class
            double meanAllClass = totalMean / meanPerClass.size();

            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.setMaximumFractionDigits(2);
            bufferedWriter.write("Berikut Hasil Pengolahan Nilai :");
            bufferedWriter.newLine();
            bufferedWriter.newLine();
            bufferedWriter.write("Berikut hasil sebaran data nilai");
            bufferedWriter.newLine();
            bufferedWriter.write("Mean : " + decimalFormat.format(meanAllClass));
            bufferedWriter.newLine();
            bufferedWriter.write("Median : " + decimalFormat.format(calculator.getMedian(listAllMean)));
            bufferedWriter.newLine();
            bufferedWriter.write("Modus : " + calculator.getModus(listAllMean));
            bufferedWriter.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
