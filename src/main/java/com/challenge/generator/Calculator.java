package com.challenge.generator;

import com.challenge.data.DataClass;
import java.util.HashMap;
import java.util.List;

public interface Calculator {
    int getFrequency(List<Integer> rawGrades, int grades);

    HashMap<String, Double> getMeanPerClass(List<DataClass> dataClassList);

    double getMedian(List<Double> values);

    long getModus(List<Double> listAllMean);
}

