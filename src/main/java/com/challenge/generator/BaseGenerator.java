package com.challenge.generator;

import com.challenge.data.DataClass;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class BaseGenerator {
    protected List<DataClass> dataClasses = new ArrayList<>();

    protected BaseGenerator() {
        readFile();
    }

    protected abstract void generate();

    private void readFile() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(Constant.FILE_LOCATION))) {
            String line;

            // reading from file for each line
            while ((line = bufferedReader.readLine()) != null) {
                List<String> rawData = Arrays.asList(line.split(";"));
                // save as Object
                DataClass dataClass = new DataClass();
                // caching grades for every class
                List<Integer> grades = new ArrayList<>();
                for (int i = 0; i < rawData.size(); i++) {
                    if (i == 0) {
                        dataClass.setClassName(rawData.get(i));
                        dataClass.setGrades(new ArrayList<>());
                    } else {
                        // set grades one by one
                        grades.add(Integer.valueOf(rawData.get(i)));
                    }
                }
                dataClass.setGrades(new ArrayList<>(grades));
                dataClasses.add(dataClass);
                // reset grades after inserted DataClass to dataClassList
                grades.clear();
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
